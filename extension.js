const Main = imports.ui.main;
const Meta = imports.gi.Meta;
const Lang = imports.lang;

/** Utility functions **/
/* Note : credit to the shellshape extension and StatusTitleBar extension, from which the following 2 functions
 * are modified. https://extensions.gnome.org/extension/294/shellshape/
 * Signals are stored by the owner, storing both the target &
 * the id to clean up later.
 * 
 * Minor modifications by @emerino (we don't like obscure code)
 * Minor modifications by @ienzam (some complexity removed, setting property directly to tracked objects)
 */
function connectAndTrack(owner, name, cb) {
    if (!owner.hasOwnProperty('_WindowTitleInStatus_bound_signals')) {
        owner._WindowTitleInStatus_bound_signals = [];
    }

    let id = owner.connect(name, cb);
    owner._WindowTitleInStatus_bound_signals.push(id);
}

function disconnectTrackedSignals(owner) {
    if (!owner || !owner._WindowTitleInStatus_bound_signals) { 
        return; 
    }

    for(let i = 0; i < owner._WindowTitleInStatus_bound_signals.length; i++) {
        let id = owner._WindowTitleInStatus_bound_signals[i];
        owner.disconnect(id);
    }
    delete owner._WindowTitleInStatus_bound_signals;
}


const WindowTitleInStatus = new Lang.Class({
    Name: 'WindowTitleInStatus',

    _init: function() {
        this.connectedWindows = [];
        this.origSetText = null;
        this.appMenu = null;
    }, 

    _disconnectWindows: function() {
        for (let i = this.connectedWindows.length - 1; i >= 0; i--) {
            disconnectTrackedSignals(this.connectedWindows[i]);
        }
        this.connectedWindows = [];
    },

    _connectWindows: function(wins) {
        this.connectedWindows = wins;
        let self = this;
        for (let i = this.connectedWindows.length - 1; i >= 0; i--) {
            connectAndTrack(this.connectedWindows[i], 'notify::title', Lang.bind(this, this._onTitleChanged));
        }
    },

    _connectWindowManager: function() {
        connectAndTrack(global.window_manager, 'maximize', Lang.bind(this, this._onTitleChanged));
        connectAndTrack(global.window_manager, 'unmaximize', Lang.bind(this, this._onTitleChanged));
    },

    _disconnectWindowManager: function() {
        disconnectTrackedSignals(global.window_manager);
    },

    _getAppTitle: function() {
        let app = this.appMenu._targetApp;
        let wins = app.get_windows();

        this._disconnectWindows();
        this._connectWindows(wins);

        let maximizedFlags = Meta.MaximizeFlags.HORIZONTAL | Meta.MaximizeFlags.VERTICAL;
        for (let i = 0; i < wins.length; i++) {
            let win = wins[i];
            if (win.get_maximized() == maximizedFlags && win.get_title() !== '') {
                return win.get_title();
            }
        }

        return app.get_name();
    },

    _onTitleChanged: function() {
        return this.origSetText.call(this.appMenu._label, this._getAppTitle());
    },

    enable: function() {
        this.appMenu = Main.panel._appMenu;
        this.origSetText = this.appMenu._label.setText;
        let self = this;
        this.appMenu._label.setText = function(text) {
            if(text !== '') {
                text = self._getAppTitle();
            }
            return self.origSetText.call(self.appMenu._label, text);
        }

        this._connectWindowManager();
        this._onTitleChanged();
    },

    disable: function() {
        this._disconnectWindows();
        this._disconnectWindowManager();
        this.appMenu._label.setText = this.origSetText;
        this.appMenu._sync();
        this.origSetText = null;
        this.appMenu = null;
    }
});

let windowTitleInStatus;

function init() {
    windowTitleInStatus = new WindowTitleInStatus();
}

function enable() {
    windowTitleInStatus.enable();
}

function disable() {
    windowTitleInStatus.disable();
}
